# Ejercicio 3

Cargar un vector con los promedios anuales (pueden tener decimales) de las notas de 10
alumnos. Informe el promedio de todas esas notas (calculado por una función) y cuantos
alumnos estuvieron por encima del promedio (calculado por otra función). Si lee del archivo
datosG04E03.txt el promedio es 7,792 y la cantidad de alumnos que superan el promedio es 5.